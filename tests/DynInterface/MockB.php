<?php
namespace Gbili\DynInterface;

class MockB implements MockBInterface
{
    public static function astatic($a)
    {
    }

    public function b($a, MockA $aobj)
    {
    }

    public function c()
    {
    }

    public static function dstatic($a, $opt = array())
    {
    }

    public function eimplemented($a, MockA $aobj)
    {
    }
}
