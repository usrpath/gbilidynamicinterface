<?php
namespace Gbili\DynInterface;

class MockC
{
    public function __construct(\StdClass $c)
    {
    }

    public static function a($a)
    {
    }
    public function bHandler($a, self $aobj)
    {
    }
    public function c()
    {
    }

    public static function dHandler($a, array $opt = array())
    {
        throw new \Exception('Some random exception');
    }

    /**
     * we are handling an instance method defined by interface, in a instance maginc way
     */
    public static function __callStatic($name, $params)
    {
        if ($name === 'b') {
        } else {
            throw new Exception\StaticMethodNotImplementedException('Not implemented');
        }
    }

    /**
     * we are handling a static method defined by interface, in a instance maginc way
     */
    public function __call($name, $params)
    {
        if ($name === 'dstatic') {
        } else {
            throw new Exception\InstanceMethodNotImplementedException('Not implemented');
        }
    }

    public function eimplemented($a, MockA $aobj)
    {
    }

}
