<?php
namespace Gbili\DynInterface;

/**
 * Wreckless magic overloading because it does not throw exception
 * if the function name is unknown
 * MockA passes the test, but this one should throw an exception
 */
class MockE extends MockA
{
    /**
     * Wreckless magic overloading because it does not throw exception
     * if the function name is unknown
     */
    public static function __callStatic($name, $params)
    {
        if ($name === 'dstatic') {
            return call_user_func_array(array(__CLASS__, 'dHandler'), $params);
        }
    }

    /**
     * Wreckless magic overloading because it does not throw exception
     * if the function name is unknown
     */
    public function __call($name, $params)
    {
        if ($name === 'b') {
            return call_user_func_array(array($this, 'bHandler'), $params);
        }
    }
}
