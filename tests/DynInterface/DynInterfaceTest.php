<?php
namespace Gbili\DynInterface;

use PHPUnit\Framework\TestCase;

class DynInterfaceTest extends TestCase
{
    public function setUp()
    {
        DynInterface::setCustomMethodNotImplementedExceptionClass(DynInterface::USING_DEFAULT_EXCEPTION);
    }

    public function testReturnsTrueWhenInterfaceIsImplementedUsingMagicOverloading()
    {
        $a = new MockA(new \StdClass);
        $this->assertEquals(true, DynInterface::dynInstanceOf($a, '\Gbili\DynInterface\MockBInterface'));
    }

    public function testThrowsAnExceptionWhenMagicOverloadingDoesNotThrowAnExceptionIfRandomMethodCall()
    {
        DynInterface::switchWrecklessMagicOverloadingCheck(true);
        $e = new MockE(new \StdClass);
        $this->expectException('Gbili\DynInterface\Exception\WrecklessMagicOverloadingException');
        DynInterface::dynInstanceOf($e, '\Gbili\DynInterface\MockBInterface');
    }


    public function testDoesNotThrowAnExceptionWhenMagicOverloadingDoesNotThrowAnExceptionIfRandomMethodCall()
    {
        DynInterface::switchWrecklessMagicOverloadingCheck(true);
        $a = new MockA(new \StdClass);
        $thrown = false;
        try {
            DynInterface::dynInstanceOf($a, '\Gbili\DynInterface\MockBInterface');
        } catch (Exception\WrecklessMagicOverloadingException $e) {
            $thrown = true;
        }
        $this->assertEquals(false, $thrown);
    }

    public function testDoesNotThrowAnExceptionWhenWrecklessMagicOverloadingCheckIsOff()
    {
        DynInterface::switchWrecklessMagicOverloadingCheck(false);
        $e = new MockE(new \StdClass);
        $thrown = false;
        try {
            DynInterface::dynInstanceOf($e, '\Gbili\DynInterface\MockBInterface');
        } catch (Exception\WrecklessMagicOverloadingException $e) {
            $thrown = true;
        }
        $this->assertEquals(false, $thrown);
    }

    public function testReturnsTrueWhenImplementsExplicitely()
    {
        $b = new MockB;
        $this->assertEquals(true, DynInterface::dynInstanceOf($b, '\Gbili\DynInterface\MockBInterface'));
    }

    public function testReturnsFalseWhenUsingMagicOverloadingWithWrongStaticallity()
    {
        $c = new MockC(new \StdClass);
        $this->assertEquals(false, DynInterface::dynInstanceOf($c, '\Gbili\DynInterface\MockBInterface'));
    }

    public function testReturnsFalseWhenUsingWrongStaticallityInNormalMethod()
    {
        $d = new MockD(new \StdClass);
        $this->assertEquals(false, DynInterface::dynInstanceOf($d, '\Gbili\DynInterface\MockBInterface'));
    }
}
