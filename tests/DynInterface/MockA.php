<?php
namespace Gbili\DynInterface;

class MockA
{
    public function __construct(\StdClass $c)
    {
    }

    public static function astatic($a)
    {
    }

    public function bHandler($a, self $aobj)
    {
    }
    public function c()
    {
    }

    public static function dHandler($a, array $opt = array())
    {
        throw new \Exception('Some random exception');
    }

    public static function __callStatic($name, $params)
    {
        if ($name === 'dstatic') {
            return call_user_func_array(array(__CLASS__, 'dHandler'), $params);
        } else {
            throw new Exception\StaticMethodNotImplementedException('Not implemented');
        }
    }

    public function __call($name, $params)
    {
        if ($name === 'b') {
            return call_user_func_array(array($this, 'bHandler'), $params);
        } else {
            throw new Exception\InstanceMethodNotImplementedException('Not implemented');
        }
    }

    public function eimplemented($a, MockA $aobj)
    {
    }
}
