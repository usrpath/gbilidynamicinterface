<?php
namespace Gbili\DynInterface;

class MockD
{
    public function __construct(\StdClass $c)
    {
    }

    public static function a($a)
    {
    }
    public function bHandler($a, self $aobj)
    {
    }
    public function c()
    {
    }

    public static function dHandler($a, array $opt = array())
    {
        throw new \Exception('Some random exception');
    }

    public static function __callStatic($name, $params)
    {
        if ($name === 'dstatic') {
        } else {
            throw new Exception\StaticMethodNotImplementedException('Not implemented');
        }
    }

    public function __call($name, $params)
    {
        if ($name === 'b') {
            return call_user_func_array(array($this, 'bHandler'), $params);
        } else {
            throw new Exception\InstanceMethodNotImplementedException('Not implemented');
        }
    }

    /**
     * we are handling an instance method defined by interface, in a static way
     */
    public static function eimplemented($a, MockA $aobj)
    {
    }

}
