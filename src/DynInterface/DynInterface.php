<?php
namespace Gbili\DynInterface;

/**
 * The goal of this class is to allow checking whether some object
 * is an instanceof a class, but in a special way: 
 * If the class is implementing some interface methods in a magic
 * overloading way (using __call()), instanceof returns false, but
 * dynInstanceOf will return true.
 *
 * When using magic overloading if the random method name is not
 * handled by an exception, and wrecklessMagicOverloadingCheck is true
 * then this will complain. To avoid the error, a magic __call() should
 * return a MethodNotImplementedException for the random method call. 
 *
 */
class DynInterface
{
    /**
     * Pass this to setCustomMethodNotImplementedExceptionClass()
     * to mean that you are using the default
     */
    const USING_DEFAULT_EXCEPTION = 12;

    const SOME_TYPE = '';
    /**
     * Method parameter has no type
     */
    const NO_TYPE = "";
    const RANDOM_METHOD_NAME = 'LrIDJIiKkjSi89jpAbLQOIEueZiwsIDncYo';

    /**
     * When true, dynInstanceOf doesn't check
     * When you pass your code to production server set this 
     * to true, because there is no point in checking if instance
     * of once you are in production. It should have been checked
     * before
     * @var bool
     */
    private static $productionFlag = false;

    /**
     * When true, dynInstanceOf check if magic methods
     * throw exceptions if unknown method name
     *
     * @var bool
     */
    private static $wrecklessMagicOverloadingCheck = true;

    /**
     * When using method magic overloading third parties
     * may define a different exception class to signify that
     * the passed method name is not suported.
     * This should be set in order to avoid false negatives.
     *
     * @var string
     */
    private static $methodNotImplementedExceptionCustomClass = null;

    /**
     * Is set in magic overépaomg when not implemented
     * and the object being tested is smart enough to throw
     * an exception of this type when not handleing a 
     * method name in __call|__callStatic
     *
     * @var Gbili\DynInterface\Exception
     */
    private static $e;

    /**
     * interface checking should be done during development not in produciton
     * dynInstanceOf() stops checking when set to true
     */
    public static function setProductionFlag($bool)
    {
        self::$productionFlag = (boolean) $bool;
    }

    /**
     * When set to true will test a random method to make sure 
     * the __call and __callStatic throw an exception for such
     * calls.
     * @param bool|int $bool
     */
    public static function switchWrecklessMagicOverloadingCheck($bool)
    {
        self::$wrecklessMagicOverloadingCheck = (boolean) $bool;
    }

    /**
     * Contains magic method overloading call
     *
     * @var Exception
     */
    public static function getException()
    {
        return self::$e;
    }

    /**
     * @param mixed $object
     * @param mixed $interface
     * @return bool
     */
    public static function dynInstanceOf($object, $interface)
    {
        if (self::$productionFlag) {
            return true;
        }

        if ($object instanceof $interface) {
            return true;
        }

        $rOClass = new \ReflectionClass($object);
        $rIClass = new \ReflectionClass($interface);

        if (is_string($object)) {
            $object = $rOClass->newInstanceWithoutConstructor();
        }

        if (self::$wrecklessMagicOverloadingCheck) {
            self::throwIfWrecklessMagicOverloading($object);
        }

        $interfaceMethods = $rIClass->getMethods(\ReflectionMethod::IS_PUBLIC);
        foreach ($interfaceMethods as $rIMethod) {
            if ($rOClass->hasMethod($rIMethod->name)) {
                if (!self::haveSameMethodDeclaration($rIMethod, $rOClass)) return false;
            } else {
                if (!self::isHandledByMagicOverloading($rOClass, $rIMethod, $object)) return false;
            }
        }
        return true;
    }

    /**
     * Given a param type name, return an object of that type
     *
     * @param string
     * @return mixed
     */
    public static function getMockparam($paramType)
    {
        switch ($paramType) {
            case 'array':
                $param = array();
                break;
            case 'bool':
                $param = true;
                break;
            case 'callable':
                $param = function () {return false;};
                break;
            case 'float':
                $param = 1.0;
                break;
            case 'int':
                $param = 1;
                break;
            case 'iterable':
                $param = new \ArrayObject(); 
                break;
            case 'object':
                $param = new \StdClass(); 
                break;
            case 'string':
                $param = '';
                break;
            default:
                $rc= new \ReflectionClass($paramType);
                if ($rc->isInterface() || $rc->isAbstract()) {
                    throw new Exception(
                        'Sadly we cannot handle the cases where a param is of an'
                        . ' interface type, don\'t use magic in those cases '
                        . ' hardcode the method'
                    ); } $param = $rc->newInstanceWithoutConstructor();
                break;
        }

        return $param;
    }

    /**
     * @param mixed
     * @throws WrecklessMagicOverloadingException
     */
    public static function throwIfWrecklessMagicOverloading($object)
    {
        $rOClass = new \ReflectionClass($object);
        if (is_string($object)) {
            $object = $rOClass->newInstanceWithoutConstructor();
        }
        $wrecklessOverloading = false;
        try {
            if ($rOClass->hasMethod('__callStatic')) {
                $wrecklessOverloading = true;
                call_user_func_array(array($rOClass->getName(), self::RANDOM_METHOD_NAME), array());
            }
            if ($rOClass->hasMethod('__call')) {
                $wrecklessOverloading = true;
                call_user_func_array(array($object, self::RANDOM_METHOD_NAME), array());
            }
        } catch (Exception\MethodNotImplementedException $e) {
            $wrecklessOverloading = false;
        } catch (\Exception $e) {
            $wrecklessOverloading = !self::isCustomMethodNotImplementedException($e);
        }
        if ($wrecklessOverloading) {
            throw new Exception\WrecklessMagicOverloadingException(
                'You are doing WrecklessOverloading, you should be ashamed!'
                . ' Add a switch statment for each method name you are handleing'
                . ' AND a DEFAULT CASE at the end that throws and exception'
                . ' when the method name is not known'
            );
        }
    }

    /**
     * @param \ReflctionMethod
     * @param \ReflctionClass
     * @return boolean
     */
    public static function haveSameMethodDeclaration(\ReflectionMethod $rIMethod, \ReflectionClass $rOClass)
    {
        $rOMethod = $rOClass->getMethod($rIMethod->name);

        if ($rIMethod->isStatic() !== $rOMethod->isStatic()) {
            return false;
        }

        $interfaceParams = $rIMethod->getParameters();
        $rOMethod = $rOClass->getMethod($rIMethod->name);
        $objectParams = $rOMethod->getParameters();
        $paramsCount = count($interfaceParams);

        if (count($objectParams) !== $paramsCount) {
            return false;
        }

        for ($i=0; $i<$paramsCount; $i++) {
            if ((string) $objectParams[$i]->getType() === self::NO_TYPE) continue;
            if ((string) $objectParams[$i]->getType() !== (string) $interfaceParams[$i]->getType()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Why, here, are we considering an \Exception() as good, and not in
     * throwIfWrecklessMagicOverloading()? Because we first check
     * throwIfWrecklessMagicOverloading() and if it does not throw, then
     * it means that unhandled methods are effectively being rejected and
     * the magic overloading throws exceptions for unexpected methods. 
     * So if the codes goes throw here, then we are not in Wreckles Magic
     * Overloading, and an exception other than 
     * 
     */
    public static function isHandledByMagicOverloading($rOClass, $rIMethod, $object)
    {
        $isHandled = false;

        $handle = ($rIMethod->isStatic()) ? $rOClass->getName() : $object;

        if (!is_callable(array($handle, $rIMethod->name))) {
            return $isHandled;
        }

        $paramsList = array();
        foreach ($rIMethod->getParameters() as $param) {
            if ($param->isOptional()) break;
            $paramsList[] = ($param->hasType()) ? self::getMockParam((string) $param->getType()) : self::SOME_TYPE;
        }

        try {
            $isHandled = true;
            call_user_func_array(array($handle, $rIMethod->name), $paramsList);
        } catch (Exception\MethodNotImplementedException $e) {
            self::$e = new Exception(
                'Called : ' . $rIMethod->name . ' on ' . print_r($handle, true) 
                . ' and __call|__callStatic has thrown an Exception\*MethodNotImplementedException.' . PHP_EOL 
                . 'Trace was : ' . PHP_EOL . $e->getTraceAsString()
            );
            $isHandled = false;
        } catch (\Exception $e) {
            $isHandled = self::isCustomMethodNotImplementedException($e);
        }
        return $isHandled;
    }

    /**
     * The exception passed represents an exception for when there is
     * no magic overloading intended for the method
     *
     * @param Exception $e
     * @return boolean
     */
    private static function isCustomMethodNotImplementedException($e)
    {
        if (self::$methodNotImplementedExceptionCustomClass === null) {
            throw new Exception\RuntimeException(
                'Magic overloading threw an unexpected exception of type : ' . get_class($e)
                . ' and you have not set a setCustomMethodNotImplementedExceptionClass($className),'
                . ' so there is no way to know whether it means method not implemented exception,' 
                . ' or some other unrelated exception.' . PHP_EOL 
                . ' Please call setCustomMethodNotImplementedExceptionClass(self::USING_DEFAULT) ' . PHP_EOL
                . ' if you are not planning to use a custom and you are using the default.' . PHP_EOL
                . 'Message was : ' . $e->getMessage() . PHP_EOL
                . 'Stack trace : ' . PHP_EOL
                . $e->getTraceAsString()
            );
        }
        $isHandled = !($e instanceof self::$methodNotImplementedExceptionCustomClass);
        return $isHandled;
    }

    /**
     * Allow passing a specific class for method not impelemented exception
     * This allows more flexibility for third party integration
     *
     * Always call this method to avoid exception
     *
     * @param mixed $className
     */
    public static function setCustomMethodNotImplementedExceptionClass($className)
    {
        if (is_int($className) && $className == self::USING_DEFAULT_EXCEPTION) {
            $className = 'Gbili\DynInterface\Exception\MethodNotImplementedException';
        }
        self::$methodNotImplementedExceptionCustomClass = $className;
    }
}
