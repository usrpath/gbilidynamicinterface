<?php
namespace Gbili\DynInterface\Exception;

class StaticMethodNotImplementedException extends MethodNotImplementedException
{
}
