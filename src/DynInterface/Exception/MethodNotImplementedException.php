<?php
namespace Gbili\DynInterface\Exception;

use Gbili\DynInterface\Exception;

class MethodNotImplementedException extends Exception
{
}
