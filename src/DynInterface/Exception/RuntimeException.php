<?php
namespace Gbili\DynInterface\Exception;

use Gbili\DynInterface\Exception;

class RuntimeException extends Exception
{
}
